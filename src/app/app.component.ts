import {Component, OnInit, ViewChild} from "@angular/core";
import {NavitiaService} from "./service/navitia.service";
import {StationModel} from "./model/station.model";
import {FormControl} from "@angular/forms";
import {Md2Datepicker} from "md2";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Itinéraire RATP';

  departureControl = new FormControl();

  departureCompletion = [];

  destinationControl = new FormControl();

  destinationCompletion = [];

  @ViewChild('datetimepicker')
  datetimepicker: Md2Datepicker;

  journeys = [];

  constructor(private navitiaService: NavitiaService) {
  }

  ngOnInit(): void {
    this.departureControl.valueChanges
      .flatMap(val => this.navitiaService.findStations(val))
      .subscribe(stations => this.departureCompletion = stations);

    this.destinationControl.valueChanges
      .flatMap(val => this.navitiaService.findStations(val))
      .subscribe(stations => this.destinationCompletion = stations);
  }

  displayStation(station: StationModel) {
    return station ? station.name : station;
  }

  recherche() {
    this.navitiaService.findJourneys(
      this.departureControl.value,
      this.destinationControl.value,
      this.datetimepicker.selected
    )
      .subscribe(journeys => this.journeys = journeys);
  }
}
