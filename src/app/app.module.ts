import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MdAutocompleteModule,
  MdButtonModule,
  MdCardModule,
  MdDatepickerModule,
  MdIconModule,
  MdInputModule,
  MdListModule,
  MdNativeDateModule,
  MdToolbarModule
} from "@angular/material";
import "hammerjs";
import "tinycolor2";
import {NavitiaService} from "./service/navitia.service";
import {JourneyComponent} from "./journey/journey.component";
import {SectionComponent} from "./section/section.component";
import {Md2Module} from "md2";

@NgModule({
  declarations: [
    AppComponent,
    JourneyComponent,
    SectionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdToolbarModule,
    MdInputModule,
    MdAutocompleteModule,
    MdButtonModule,
    MdCardModule,
    MdIconModule,
    MdDatepickerModule,
    MdNativeDateModule,
    MdListModule,
    Md2Module
  ],
  providers: [
    NavitiaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
