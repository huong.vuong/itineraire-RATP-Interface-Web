import {Component, Input, OnInit} from "@angular/core";
import {SectionModel} from "../model/section.model";
import {SectionType} from "../model/section.type";

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {

  @Input()
  section: SectionModel;

  constructor() {
  }

  ngOnInit() {
  }

  picto(): string {
    if (this.section.type === SectionType.waiting) {
      return 'https://canaltp.github.io/navitia-playground/img/duration.svg';
    }
    if (this.section.type === SectionType.transfer || (this.section.mode && this.section.mode === 'walking')) {
      return 'https://canaltp.github.io/navitia-playground/img/pictos/Walking.svg';
    }
    if (this.section.display_informations) {
      if (this.section.display_informations.physical_mode.includes('Train')) {
        return 'https://canaltp.github.io/navitia-playground/img/pictos/Train.svg';
      }
      if (this.section.display_informations.physical_mode.includes('Bus')) {
        return 'https://canaltp.github.io/navitia-playground/img/pictos/Bus.svg';
      }
      if (this.section.display_informations.physical_mode.includes('Métro')) {
        return 'https://canaltp.github.io/navitia-playground/img/pictos/Metro.svg';
      }
    }
    return 'https://canaltp.github.io/navitia-playground/img/pictos/Train.svg';
  }
}
