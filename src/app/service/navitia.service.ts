import {Injectable} from "@angular/core";
import {StationModel} from "../model/station.model";
import {Headers, Http, RequestOptions, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {JourneyModel} from "../model/journey.model";
import * as moment from "moment";

@Injectable()
export class NavitiaService {

  private apiKey = 'aa3a0d64-8121-47bc-b1b1-bf61b134cbb8';

  private apiUrl = 'https://api.navitia.io/v1/coverage/fr-idf/';

  constructor(private http: Http) {
  }

  findStations(query: string): Observable<StationModel[]> {
    const headers = new Headers({'Authorization': this.apiKey});

    const params = new URLSearchParams();
    params.set('q', query);
    params.set('type[]', 'stop_area');

    const options = new RequestOptions({headers: headers, search: params});

    return this.http.get(this.apiUrl + 'pt_objects', options)
      .map(response => response.json())
      .map(body => body.pt_objects ? body.pt_objects.map(json => new StationModel(json.id, json.name)) : [])
      .catch(this.handleError);
  }

  findJourneys(from: StationModel, to: StationModel, date: Date): Observable<JourneyModel[]> {
    const headers = new Headers({'Authorization': this.apiKey});

    const params = new URLSearchParams();
    params.set('from', from.id);
    params.set('to', to.id);
    params.set('datetime', moment(date).format('YYYYMMDDTHHmmss'));

    const options = new RequestOptions({headers: headers, search: params});

    return this.http.get(this.apiUrl + 'journeys', options)
      .map(response => response.json())
      .map(body => body.journeys ? body.journeys.map(json => new JourneyModel(json)) : [])
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    console.error(error);
    return Observable.of([]);
  }
}
