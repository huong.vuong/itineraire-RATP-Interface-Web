export enum SectionType {
  public_transport,
  street_network,
  waiting,
  stay_in,
  transfer,
  crow_fly,
  on_demand_transport,
  bss_rent,
  bss_put_back,
  boarding,
  landing
}
