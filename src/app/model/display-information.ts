import * as tinycolor from "tinycolor2";

export class DisplayInformation {
  network: string;
  direction: string;
  commercial_mode: string;
  physical_mode: string;
  label: string;
  color: tinycolorInstance;
  code: string;
  description: string;
  equipments: string[];

  constructor(json: any) {
    this.network = json.network ? json.network : '';
    this.direction = json.direction ? json.direction : '';
    this.commercial_mode = json.commercial_mode ? json.commercial_mode : '';
    this.physical_mode = json.physical_mode ? json.physical_mode : '';
    this.label = json.label ? json.label : '';
    this.color = json.color ? tinycolor('#' + json.color) : null;
    this.code = json.code ? json.code : '';
    this.description = json.description ? json.description : '';
    this.equipments = json.equipments ? json.equipments : [];
  }
}
