import {SectionModel} from "./section.model";
import * as moment from "moment";

export class JourneyModel {

  public duration: moment.Duration;
  public nb_transfers: number;
  public departure_date_time: Date;
  public requested_date_time: Date;
  public arrival_date_time: Date;
  public sections: SectionModel[];

  constructor(json: any) {
    this.duration = moment.duration(json.duration, 'seconds');
    this.nb_transfers = json.nb_transfers;
    this.departure_date_time = moment(json.departure_date_time, 'YYYYMMDDTHHmmss').toDate();
    this.requested_date_time = moment(json.requested_date_time, 'YYYYMMDDTHHmmss').toDate();
    this.arrival_date_time = moment(json.arrival_date_time, 'YYYYMMDDTHHmmss').toDate();
    this.sections = json.sections ? json.sections.map(it => new SectionModel(it)) : [];
  }
}

