import {PlaceModel} from "./place.model";

import * as moment from "moment";
import {SectionType} from "./section.type";
import {DisplayInformation} from "./display-information";

export class SectionModel {
  id: string;
  duration: moment.Duration;
  from: PlaceModel;
  to: PlaceModel;
  departure_date_time: Date;
  arrival_date_time: Date;
  type: SectionType;
  display_informations: DisplayInformation;
  mode: string;

  constructor(json: any) {
    this.id = json.id;
    this.duration = moment.duration(json.duration, 'seconds');
    this.from = json.from ? new PlaceModel(json.from) : null;
    this.to = json.to ? new PlaceModel(json.to) : null;
    this.departure_date_time = moment(json.departure_date_time, 'YYYYMMDDTHHmmss').toDate();
    this.arrival_date_time = moment(json.arrival_date_time, 'YYYYMMDDTHHmmss').toDate();
    this.type = SectionType[<string>json.type];
    this.display_informations = json.display_informations ? new DisplayInformation(json.display_informations) : null;
    this.mode = json.mode;
  }
}
