import { ItineraireRATPPage } from './app.po';

describe('itineraire-ratp App', () => {
  let page: ItineraireRATPPage;

  beforeEach(() => {
    page = new ItineraireRATPPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
